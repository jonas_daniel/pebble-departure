/*
Departure Monitor
Copyright (C) 2014 Andreas Bauer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pebble.h>

#include <departure.h>
#include "networks.h"

static Window *window;

static MenuLayer *menu_layer;

static Country ctry = 0;

static void
menu_select_callback(MenuLayer *menu_layer, MenuIndex *cell_index, void *data)
{
  if(CurrentView == VIEW_NETWORK1)
    {

      ctry = SortedCountries[cell_index->row];

      CurrentView = VIEW_NETWORK2;

      menu_layer_reload_data(menu_layer);
      menu_layer_set_selected_index(menu_layer, (MenuIndex) {.row = 0, .section=0}, MenuRowAlignBottom, false);
      layer_mark_dirty(menu_layer_get_layer(menu_layer));
    }
  else if(CurrentView == VIEW_NETWORK2)
    {
      Network oldProvider = Provider;
      Network newProvider = getNetwork(ctry, cell_index->row);

      if (oldProvider != newProvider)
        {
          reset();
          Provider = newProvider;
        }

      window_stack_pop(false);
      if (oldProvider == 0)
        departure_show_window();
    }

}

static uint16_t
menu_get_num_sections_callback(MenuLayer *menu_layer, void *data)
{
  return 1;
}

static uint16_t
menu_get_num_rows_callback(MenuLayer *menu_layer, uint16_t section_index, void *data)
{
  if(CurrentView == VIEW_NETWORK1)
    {
      return 10;
    }
  else if(CurrentView == VIEW_NETWORK2)
    {
      return getNetworkNumber(ctry);
    }
  else
    {
      return 0;
    }
}

static int16_t
menu_get_header_height_callback(MenuLayer *menu_layer, uint16_t section_index, void *data)
{
  return MENU_CELL_BASIC_HEADER_HEIGHT;
}

static int16_t
menu_get_cell_height_callback(MenuLayer *menu_layer, MenuIndex *cell_index, void *data)
{
  return 24;
}

static void
menu_draw_header_callback(GContext* ctx, const Layer *cell_layer, uint16_t section_index, void *data)
{
  if(CurrentView == VIEW_NETWORK1)
      {
        menu_cell_basic_header_draw(ctx, cell_layer, "Country");
      }
  else if(CurrentView == VIEW_NETWORK2)
      {
        menu_cell_basic_header_draw(ctx, cell_layer, getCountryName(ctry));
      }


}

// This is the menu item draw callback where you specify what each item should look like
static void
menu_draw_row_callback(GContext* ctx, const Layer *cell_layer, MenuIndex *cell_index, void *data)
{
  if(CurrentView == VIEW_NETWORK1)
    {
      menu_cell_basic_draw(ctx, cell_layer, getCountryName(SortedCountries[cell_index->row]), NULL,NULL);
    }
  else if(CurrentView == VIEW_NETWORK2)
    {
      Network net = getNetwork(ctry, cell_index->row);

      //menu_cell_basic_header_draw(ctx, cell_layer, getNetworkName(net));
      //menu_cell_title_draw(ctx, cell_layer, getNetworkName(net));
      menu_cell_basic_draw(ctx, cell_layer, getNetworkName(net), NULL, NULL);
    }

}

static void
window_load(Window *window)
{
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);
  // Create the menu layer
  menu_layer = menu_layer_create(bounds);

  // Set all the callbacks for the menu layer
  menu_layer_set_callbacks(menu_layer, NULL, (MenuLayerCallbacks
        )
          { .get_num_sections = menu_get_num_sections_callback, .get_num_rows = menu_get_num_rows_callback, .get_header_height =
              menu_get_header_height_callback, .draw_header = menu_draw_header_callback, .draw_row = menu_draw_row_callback, .select_click =
              menu_select_callback});
              //, .get_cell_height = menu_get_cell_height_callback});

  // Bind the menu layer's click config provider to the window for interactivity
  menu_layer_set_click_config_onto_window(menu_layer, window);

  // Add it to the window for display
  layer_add_child(window_layer, menu_layer_get_layer(menu_layer));

}

static void
window_appear(Window *window)
{

  CurrentView = VIEW_NETWORK1;

  menu_layer_reload_data(menu_layer);
  menu_layer_set_selected_index(menu_layer, (MenuIndex) {.row = 0, .section=0}, MenuRowAlignBottom, false);
  layer_mark_dirty(menu_layer_get_layer(menu_layer));

}

static void
window_disappear(Window *window)
{

}

static void
window_unload(Window *window)
{
  menu_layer_destroy(menu_layer);

}

void
settings_show_window()
{
  window_stack_push(window, true);

}

void
settings_init(void)
{
  window = window_create();
  window_set_window_handlers(window, (WindowHandlers
        )
          { .load = window_load, .unload = window_unload, .appear = window_appear, .disappear = window_disappear});
}

void
settings_deinit(void)
{
  window_destroy(window);
}
