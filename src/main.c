/*
Departure Monitor
Copyright (C) 2014 Andreas Bauer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pebble.h>

#include <departure.h>

station CurrentStation;
station SavedStations[MAX_STATIONS];
station NearbyStations[MAX_STATIONS];

int8_t NumSavedStations = 0;
int8_t NumNearbyStations = 0;

int8_t NextSavedStation = 0;

int8_t DepartureState = 0;

int8_t JSReady = 0;
int8_t CurrentView = VIEW_NONE;

int Provider = 0;

departure Departures[MAX_DEPARTURES];
int8_t NumDepartures;

static void
in_received_handler(DictionaryIterator *iter, void *context)
{

  Tuple *msg = dict_find(iter, KEY_JSREADY);
  if (msg)
    {
      JSReady = 1;

      if(CurrentView == VIEW_DEPARTURE)
        {
          send_departure_request();
        }
      else if(CurrentView == VIEW_STATIONS)
        {
          send_stations_request();
        }
      return;
    }

  msg = dict_find(iter, KEY_STATIONS);
  if (msg)
    {
      stations_message_handler(iter, context);
      return;
    }

  msg = dict_find(iter, KEY_DEPARTURES);
  if (msg)
    {
      departure_message_handler(iter, context);
      return;
    }
}

static void
in_dropped_handler(AppMessageResult reason, void *context)
{
  APP_LOG(APP_LOG_LEVEL_DEBUG, "App Message Dropped!");
  if (reason == APP_MSG_BUSY)
    APP_LOG(APP_LOG_LEVEL_DEBUG, "Busy");
  if (reason == APP_MSG_BUFFER_OVERFLOW)
    APP_LOG(APP_LOG_LEVEL_DEBUG, "buffer full");
}

static void
out_failed_handler(DictionaryIterator *failed, AppMessageResult reason, void *context)
{
  APP_LOG(APP_LOG_LEVEL_DEBUG, "App Message Failed to Send!");

  Tuple *msg = dict_find(failed, KEY_DEPARTURES);
    if (msg)
      {
        LastUpdateDepartures = 0;
      }
}

static void
app_message_init(void)
{
  // Register message handlers
  app_message_register_inbox_received(in_received_handler);
  app_message_register_inbox_dropped(in_dropped_handler);
  app_message_register_outbox_failed(out_failed_handler);
  // Init buffers
  app_message_open(1024, 256);
}

static void
init(void)
{
  DepartureState = persist_read_int(2);
  NumSavedStations = persist_read_int(3);
  NextSavedStation = persist_read_int(4);
  persist_read_data(0, SavedStations, sizeof(station) * NumSavedStations);
  persist_read_data(1, &CurrentStation, sizeof(station));
  Provider = persist_read_int(5);
  ViewMode = persist_read_int(6);

  if (DepartureState > STATE_NONE)
    DepartureState = 2;

  app_message_init();

  stations_init();
  settings_init();
  departure_init();
  about_init();

  if (Provider != 0)
    departure_show_window();

  if (Provider == 0)
    settings_show_window();

}

static void
deinit(void)
{

  if (NumSavedStations > 0)
    {
      persist_write_data(0, SavedStations, sizeof(station) * NumSavedStations);
    }
  persist_write_data(1, &CurrentStation, sizeof(station));
  if (DepartureState > STATE_NONE)
    {
      persist_write_int(2, STATE_LOADING);
    }
  else
    {
      persist_write_int(2, STATE_NONE);
    }
  persist_write_int(3, NumSavedStations);
  persist_write_int(4, NextSavedStation);
  persist_write_int(5, Provider);
  persist_write_int(6, ViewMode);

  about_deinit();
  stations_deinit();
  settings_deinit();
  departure_deinit();

}

int
main(void)
{
  init();

  APP_LOG(APP_LOG_LEVEL_DEBUG, "Done initializing");

  app_event_loop();
  deinit();
}
