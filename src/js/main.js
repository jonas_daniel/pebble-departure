/*
Departure Monitor
Copyright (C) 2014 Andreas Bauer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

var XmlDocument = require('xmldoc').XmlDocument;

var maxTriesForSendingAppMessage = 3;
var timeoutForAppMessageRetry = 3000;

var MaxStations = 5;


var Urls = { 225: 'http://efa9-5.vrn.de/dm_rbl/', 
             230: 'http://213.144.24.66/bcl/', 
             205 : 'http://efa.mvv-muenchen.de/mobile/', 
             207 : "http://efa.avv-augsburg.de/avv/",
             215 : "http://62.206.133.180/bsag/",
             214 : "http://212.68.73.240/bsvag/",
             401 : "http://www.efa-bvb.ch/bvb/",
             229 : "http://www.ding-ulm.de/ding2/",
             1600 : "http://wojhati.rta.ae/dub/",
             213 : "http://mobil.efa.de/mobile3/",
             307 : "http://efa.ivb.at/ivb/",
             1500 : "http://tpweb.atc.bo.it/atc2/",
             303 : "http://www.linzag.at/linz2/",
             1300 : "http://164.8.32.183/slo/",
             1901 : "http://jp.ptv.vic.gov.au/ptv/",
             223 : "http://mobil.mvg-online.de/",
             228 : "http://efa.naldo.de/naldo/",
             232 : "http://www.efa-bw.de/nvbw/",
             1800 : "http://tripplanner.transit.511.org/mtc/",
             308 : "http://fahrplan.verbundlinie.at/stv/",
             304 : "http://efa.svv-info.at/svv/",
             1900 : "http://tp.transportnsw.info/nsw/",
             1200 : "http://www.journeyplanner.transportforireland.ie/nta/",
             1100 : "http://journeyplanner.tfl.gov.uk/user/",
             1101 : "http://www.travelineeastmidlands.co.uk/em/",
             1103 : "http://www.travelinesw.com/swe/",
             1102 : "http://www.travelinemidlands.co.uk/wmtis/",
             231 : "http://efa.vag-freiburg.de/vagfr/",
             402 : "http://mobil.vbl.ch/vblmobil/",
             306 : "http://efaneu.vmobil.at/vvvmobile/",
             219 : "http://www.vms.de/vms2/",
             210 : "http://80.146.180.107/delfi/",
             301 : "http://depmon.vor.at:8380/vorrbl/",
             221 : "http://app.vrr.de/standard/",
             209 : "http://efa.mobilitaetsverbund.de/web/",
             218 : "http://efa.vvo-online.de:8080/dvb/",
             227 : "http://www2.vvs.de/vvs/",
             305 : "http://efa.vvt.at/vvtadr/",
             233 : "http://195.30.98.162:8081/vvv2/",
             302 : "http://www.wienerlinien.at/ogd_routing/"
            	 };
var provider = 0;

function sendAppMessage(message, numTries) {
	if (numTries < maxTriesForSendingAppMessage) 
	{
		numTries++;
                
        console.log('Sending AppMessage to Pebble: ' + JSON.stringify(message));
        Pebble.sendAppMessage(message, function() {}, function(e) {
                                console.log('Failed sending AppMessage. Error: ' + e.data.error.message);
                                setTimeout(function() {
                                        sendAppMessage(message, numTries);
                                }, timeoutForAppMessageRetry);
                        	}
        					);
    }
	else
	{
		console.log('Failed sending AppMessage. Bailing. ' + JSON.stringify(message));
    }
}

function sendError(fun, error, e)
{
 var message = {};
            message[fun] = -error;
            sendAppMessage(message,0);
             console.log("Request returned error code " + error + "; function: "+fun+"; "+e);
}


function getStationsGPS(position) {
    var req = new XMLHttpRequest();
    var lat = position.coords.latitude;
    var lon = position.coords.longitude;
    
    var url = Urls[provider]+'XML_COORD_REQUEST?coordOutputFormat=WGS84&max='+MaxStations+'&inclFilter=1&radius_1=1320&type_1=STOP&coord='+lon+':'+lat+':WGS84'
    req.open('GET', url, true);
    console.log(url);
  
    req.onload = function(e) {
           	if(req.status == 200) 
           	{
           		response = new XmlDocument( req.responseText);
           		try
           		{
           			var num = 0;
           			var results = response.childNamed('itdCoordInfoRequest').childNamed('itdCoordInfo').childNamed('coordInfoItemList')
					if(results==undefined)
					{
					    num = 0;
					}
					else
					{
					   results = results.childrenNamed('coordInfoItem');
					   num = results.length;
					   if(num > MaxStations)
					   {
						   num = MaxStations;
					   }
					}	
            
	            	var message = {};
	            	message.stations = 1;
	            	message.numitems = num;
	            	message.station_name = new Array(num);
	            	message.station_name2 = new Array(num);
	            	message.station_id = new Array(num);
	            	message.provider = provider;

	            	for(var i=0;i<num;i++)
	            	{
	                	message.station_name[i] = 100+i;
	                	message.station_name2[i] = 150+i;
	                	message.station_id[i] = 200+i;
	                	message[100+i] = results[i].attr.name.substr(0,25);
	                	message[150+i] = results[i].attr.locality.substr(0,25);
	                	message[200+i] = parseInt(results[i].attr.id);
	
	            	}
	            	sendAppMessage(message,0);
           		}
           		catch(e)
           		{
           			sendError(1,3,e);
           		}
           	} else {
           		sendError(1,2,0);    
           	}
       	};
       	
       	req.onerror = function(e) {
            	sendError(1,1,0);
       	};
       
       	req.send(null);
    
}

function getStationLocate() {
    navigator.geolocation.getCurrentPosition(getStationsGPS);
}

function getDepartures(stopID) {
    var req = new XMLHttpRequest();
    var url = Urls[provider]+'XML_DM_REQUEST?&sessionID=0&depType=stopEvents&mode=direct&limit=10&mergeDep=1&useRealtime=1&line=any&type_dm=stationID&name_dm='+stopID;
    req.open('GET', url, true);
    console.log(url);
    
      req.onload = function(e) {
    	  if(req.status == 200) 
    	  {
    	  response = new XmlDocument( req.responseText);
             var num = 0;
             try{
            	
	            var results = response.childNamed('itdDepartureMonitorRequest').childNamed('itdDepartureList');
	            if(results == undefined) 
	            {
	            	num = 0;
	            }
	            else
	            {
	                results = results.childrenNamed('itdDeparture');
	                num = results.length;
	                if(num > 10)
	                {
	                	num = 10;
	                }
	            }
	            
            
	            var message = {};
	            message.departures = 1;
	            message.numitems = num;
	            message.departure_line = new Array(num);
	            message.departure_time = new Array(num);
	            message.departure_dest = new Array(num);
	            message.departure_rt = new Array(num);
	            message.provider = provider;
	            message.station_id = stopID;
            
	            var a = response.attr.now.split(/[^0-9]/);
	            var lt = new Date(a[0],a[1]-1,a[2],a[3],a[4],a[5] );

	            message.localtime = Math.round(lt.getTime()/1000);
            
	            for(var i=0; i<num; i++)
	            {
	                var line = results[i].childNamed('itdServingLine').attr.number;
	                var date;
	                var time;
	                try{
	                	date = results[i].childNamed('itdRTDateTime').childNamed('itdDate')
			            time = results[i].childNamed('itdRTDateTime').childNamed('itdTime')
			            message.departure_rt[i] = 1;
	                }
	                catch(e) 
	                {
	                    date = results[i].childNamed('itdDateTime').childNamed('itdDate')
	    		        time = results[i].childNamed('itdDateTime').childNamed('itdTime')
	    		        message.departure_rt[i] = 0;
	                }
	                var dest = results[i].childNamed('itdServingLine').attr.direction.substr(0,25);

	                var tm = new Date();

	                tm.setFullYear(parseInt(date.attr.year));
	                tm.setMonth(parseInt(date.attr.month)-1);
	                tm.setDate(parseInt(date.attr.day));
	
	                tm.setHours(parseInt(time.attr.hour));
	                tm.setMinutes(parseInt(time.attr.minute));
	                tm.setSeconds(0);
                
                
	                message.departure_line[i] = 100+i;
	                message.departure_time[i] = 150+i;
	                message.departure_dest[i] = 200+i;
	                
	                message[100+i] = line;
	                message[150+i] = Math.round(tm.getTime()/1000);
	                message[200+i] = dest;

	            }
	            sendAppMessage(message,0);
            }
            catch(e) 
            {
            	sendError(4,3,e);	
            }
          
    	  }
    	  else
    	  {
    		  sendError(4,2,0);
          }	
     
       };
       
       req.onerror = function(e) {
           sendError(4,1,0);
       };
       
       req.send(null);
    
}


Pebble.addEventListener('appmessage', function(e) {
        console.log('AppMessage received from Pebble: ' + JSON.stringify(e.payload));
        
        var request = e.payload.stations;
        if(request) {   
        provider = e.payload.provider;
            getStationLocate();
            return;
        }
        var dm = e.payload.departures;
        if(dm) {
        provider = e.payload.provider;
            getDepartures(e.payload.station_id);
            return;
        }

});

Pebble.addEventListener("ready",
    function(e) {
    	var message = {};
    	message.jsready = 1;
    	sendAppMessage(message,0);
    }
);

