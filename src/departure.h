/*
Departure Monitor
Copyright (C) 2014 Andreas Bauer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define NAME_LENGTH 26
#define LINE_LENGTH 20
#define TIME_LENGTH 10

#define MAX_STATIONS 10
#define MAX_DEPARTURES 10

#define REFRESH_INTERVAL 30

#define STATE_NONE 0
#define STATE_READY 1
#define STATE_LOADING 2
#define STATE_ERROR -1

typedef struct
{
  int32_t id;
  char name[NAME_LENGTH];
  char name2[NAME_LENGTH];
} station;

typedef struct
{
  char line[LINE_LENGTH];
  char dest[NAME_LENGTH];
  char time_str[TIME_LENGTH];
  int8_t rt;
  time_t time;
} departure;

extern station CurrentStation;
extern station SavedStations[MAX_STATIONS];
extern station NearbyStations[MAX_STATIONS];

extern int8_t DepartureState;
extern int Provider;

extern int8_t NumSavedStations;
extern int8_t NumNearbyStations;
extern int8_t NextSavedStation;

extern departure Departures[MAX_DEPARTURES];
extern int8_t NumDepartures;

extern time_t LastUpdateDepartures;
extern int8_t ViewMode;

extern int8_t StationsState;

extern int8_t JSReady;
extern int8_t CurrentView;

void
reset();
void
set_station(station* new_station);
void
save_current_station();
void
send_departure_request();
void
departure_show_window();
void
departure_init();
void
departure_deinit();
void
departure_message_handler(DictionaryIterator *iter, void *context);

void
stations_show_window();
void
stations_init();
void
stations_deinit();
void
stations_message_handler(DictionaryIterator *iter, void *context);
void send_stations_request();

void
settings_show_window();
void
settings_init();
void
settings_deinit();


void
about_show_window();
void
about_init();
void
about_deinit();

char*
itoa(int num, char* buff);

enum
{
  KEY_DUMMY = 0x0,
  KEY_STATIONS = 0x1,
  KEY_STATION_ID = 0x2,
  KEY_STATION_NAME = 0x3,
  KEY_STATION_NAME2 = 0x9,
  KEY_DEPARTURES = 0x4,
  KEY_DEPARTURE_LINE = 0x5,
  KEY_DEPARTURE_TIME = 0x6,
  KEY_DEPARTURE_DEST = 0x7,
  KEY_NUMITEMS = 0x8,
  KEY_LOCALTIME = 0xa,
  KEY_PROVIDER = 0xb,
  KEY_JSREADY = 0xc,
  KEY_DEPARTURE_RT = 0xd,
};

enum
{
  VIEW_NONE = 0x0,
  VIEW_DEPARTURE = 0x1,
  VIEW_STATIONS = 0x2,
  VIEW_NETWORK1 = 0x3,
  VIEW_NETWORK2 = 0x4,
};

enum
{
  VIEW_SHORT = 0x0,
  VIEW_LONG = 0x1,
};
