/*
Departure Monitor
Copyright (C) 2014 Andreas Bauer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "networks.h"

Country SortedCountries[] =
  { Australia, Austria, Germany, Italy, Ireland, UAE, UK, USA,
      Slovenia, Switzerland };

const char*
getCountryName(Country index)
{
  switch (index)
    {
  case Australia:
    return "Australia";
  case Austria:
    return "Austria";
  case Belgium:
    return "Belgium";
  case Denmark:
    return "Denmark";
  case Europe:
    return "Europe";
  case Germany:
    return "Germany";
  case Italy:
    return "Italy";
  case Ireland:
    return "Ireland";
  case Israel:
    return "Israel";
  case Luxembourg:
    return "Luxembourg";
  case Netherlands:
    return "Netherlands";
  case Norway:
    return "Norway";
  case Poland:
    return "Poland";
  case UAE:
    return "UAE";
  case UK:
    return "UK";
  case USA:
    return "USA";
  case Slovenia:
    return "Slovenia";
  case Sweden:
    return "Sweden";
  case Switzerland:
    return "Switzerland";
  default:
    return "";
    };

}

const char*
getNetworkName(Network network)
{
  switch (network)
    {
  case VRN:
    return "VRN";
  case KVV:
    return "KVV";
  case MVV:
    return "MVV";
  case ATC:
    return "ATC";
  case AVV:
    return "AVV";
  case BSAG:
    return "BSAG";
  case BSVAG:
    return "BSVAG";
  case BVB:
    return "BVB";
  case DING:
    return "DING";
  case DUB:
    return "DUB";
  case GVH:
    return "GVH";
  case IVB:
    return "IVB";
  case LINZ:
    return "LINZ";
  case MARIBOR:
    return "MARIBOR";
  case MET:
    return "MET";
  case MVG:
    return "MVG";
  case NALDO:
    return "NALDO";
  case NVBW:
    return "NVBW";
  case SF:
    return "SF";
  case STV:
    return "STV";
  case SVV:
    return "SVV";
  case SYDNEY:
    return "SYDNEY";
  case TFI:
    return "TFI";
  case TFL:
    return "London";
  case TLEM:
    return "TLEM";
  case TLSW:
    return "TLSW";
  case TLWM:
    return "TLWM";
  case VAGFR:
    return "VAGFR";
  case VBL:
    return "VBL";
  case VGN:
    return "VGN";
  case VMOBIL:
    return "VMOBIL";
  case VMS:
    return "VMS";
  case VMV:
    return "VMV";
  case VOR:
    return "VOR";
  case VRR:
    return "VRR";
  case VVM:
    return "VVM";
  case VVO:
    return "VVO";
  case VVS:
    return "VVS";
  case VVT:
    return "VVT";
  case VVV:
    return "VVV";
  case WIEN:
    return "WIEN";
  default:
    return "";
    }
}
int
getNetworkNumber(Country country)
{
  switch (country)
    {
  case Australia:
    return 2;
  case Austria:
    return 8;
  case Germany:
    return 19;
  case Italy:
    return 1;
  case Ireland:
    return 1;
  case UAE:
    return 1;
  case UK:
    return 4;
  case USA:
    return 1;
  case Switzerland:
    return 2;
  case Slovenia:
    return 1;
  default:
    return 0;
    };
}

Network
getNetwork(Country country, int index)
{
  switch (country)
    {
  case Australia:
    ;
      {
        Network n[] =
          { MET, SYDNEY };
        return n[index];
      }

  case Austria:
    ;
      {
        Network n[] =
          { IVB, LINZ, STV, SVV, VMOBIL, VOR, VVT, WIEN };
        return n[index];
      }

  case Germany:
    ;
      {
        Network n[] =
          { AVV, BSAG, BSVAG, DING, GVH, KVV, MVG, MVV, NALDO, NVBW, VAGFR, VMS, VMV, VRN, VRR, VVM, VVO, VVS, VVV };
        return n[index];
      }

  case Italy:
    ;
      {
        Network n[] =
          { ATC };
        return n[index];
      }

  case Ireland:
    ;
      {
        Network n[] =
          { TFI };
        return n[index];
      }

  case UAE:
    ;
      {
        Network n[] =
          { DUB };
        return n[index];
      }

  case UK:
    ;
      {
        Network n[] =
          { TFL, TLEM, TLSW, TLWM };
        return n[index];
      }

  case USA:
    ;
      {
        Network n[] =
          { SF };
        return n[index];
      }

  case Switzerland:
    ;
      {
        Network n[] =
          { BVB, VBL };
        return n[index];
      }
  case Slovenia:
    ;
      {
        Network n[] =
          { MARIBOR };
        return n[index];
      }
  default:
    return NONE;
    };
}

